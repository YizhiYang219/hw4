/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mapeditor.gui;


import java.awt.Color;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Font;
import mapeditor.MapEditorApp;
import mapeditor.data.DataManager;
import mapeditor.data.Subregion;
import mapeditor.workspaceController.WorkspaceController;
import properties_manager.PropertiesManager;
import saf.components.AppWorkspaceComponent;
import static saf.settings.AppPropertyType.NEW_ICON;
import static saf.settings.AppPropertyType.NEW_TOOLTIP;
import static saf.settings.AppPropertyType.CHANGEMAPNAME_TOOLTIP;
import static saf.settings.AppPropertyType.ADDIMAGETOMAP_TOOLTIP;
import static saf.settings.AppPropertyType.REMOVEIMAGEFROMMAP_TOOLTIP;
import static saf.settings.AppPropertyType.CHANGEMAPBACKGROUNDCOLOR_TOOLTIP;
import static saf.settings.AppPropertyType.CHANGEBORDERCOLOR_TOOLTIP;
import static saf.settings.AppPropertyType.CHANGEBORDERTHICKNESS_TOOLTIP;
import static saf.settings.AppPropertyType.REASSIGNMAPCOLORS_TOOLTIP;
import static saf.settings.AppPropertyType.PLAYSUBREGIONANTHEM_TOOLTIP;
import static saf.settings.AppPropertyType.ZOOMMAPINOUT_TOOLTIP;
import static saf.settings.AppPropertyType.CHANGEMAPDIMENSIONS_TOOLTIP;
import static saf.settings.AppPropertyType.CHANGE_MAP_NAME;
import static saf.settings.AppPropertyType.ADD_IMAGE_TO_MAP;
import static saf.settings.AppPropertyType.REMOVE_IMAGE_ON_MAP;
import static saf.settings.AppPropertyType.CHANGE_MAP_BACKGROUND_COLOR;
import static saf.settings.AppPropertyType.CHANGE_BORDER_COLOR;
import static saf.settings.AppPropertyType.CHANGE_BORDER_THICKNESS;
import static saf.settings.AppPropertyType.REASSIGN_MAP_COLORS;
import static saf.settings.AppPropertyType.PLAY_SUB_REGION_ANTHEM;
import static saf.settings.AppPropertyType.CHANGE_MAP_DIMENSIONS;

import saf.ui.AppGUI;

/**
 *
 * @author McKillaGorilla
 */
public class Workspace extends AppWorkspaceComponent {

    MapEditorApp app;

    AppGUI gui;

    Button changeMapName;
    Button addImageToMap;
    Button removeImageOnMap;
    Button changeMapBackgroundColor;
    Button changeBorderColor;
    Button changeBorderThickness;
    Button reassignMapColors;
    Button playSubregionAnthem;
    Slider thicknessSlider;
    Slider zoomSlider;
    Button changeMapDimensions;
    SplitPane splitPane;
   
    TableColumn nameColumn;
    TableColumn leaderColumn;
    TableColumn capitalColumn;

    Pane left;
    TableView<Subregion> itemsTable;

    public Workspace(MapEditorApp initApp) {

        app = initApp;

        gui = app.getGUI();


        workspace = new Pane();

        splitPane = new SplitPane();

        left = new Pane();

        itemsTable = new TableView<>();

        activateWorkspace(app.getGUI().getAppPane());
        
        
       
        
        layoutGUI();
    }

    @Override
    public void reloadWorkspace() {

        
        DataManager data = (DataManager) app.getDataComponent();

        ArrayList<Polygon> polygons = data.getPolygons();

        for (int i = 0; i < polygons.size(); i++) {

            ((Pane) splitPane.getItems().get(0)).getChildren().add(polygons.get(i));
        }
        
        System.out.println(workspace.getWidth());
    }

    @Override
    public void initStyle() {

    }

    private void layoutGUI() {

       
        HBox hbox = new HBox();

        hbox.setMinWidth(100);
        ((FlowPane) app.getGUI().getAppPane().getTop()).getChildren().add(hbox);

        Pane top = (Pane) app.getGUI().getAppPane().getTop();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        changeMapName = app.getGUI().initChildButton(top, CHANGE_MAP_NAME.toString(), CHANGEMAPNAME_TOOLTIP.toString(), false);
        addImageToMap = app.getGUI().initChildButton(top, ADD_IMAGE_TO_MAP.toString(), ADDIMAGETOMAP_TOOLTIP.toString(), false);
        removeImageOnMap = app.getGUI().initChildButton(top, REMOVE_IMAGE_ON_MAP.toString(), REMOVEIMAGEFROMMAP_TOOLTIP.toString(), false);
        changeMapBackgroundColor = app.getGUI().initChildButton(top, CHANGE_MAP_BACKGROUND_COLOR.toString(), CHANGEMAPBACKGROUNDCOLOR_TOOLTIP.toString(), false);
        changeBorderColor = app.getGUI().initChildButton(top, CHANGE_BORDER_COLOR.toString(), CHANGEBORDERCOLOR_TOOLTIP.toString(), false);
        Label thickness = new Label("  Thickness:   ");
        thickness.getStyleClass().add("cust");
        top.getChildren().add(thickness);
        thicknessSlider = new Slider();
        thicknessSlider.setMaxSize(0, 100);
        top.getChildren().add(thicknessSlider);
        Tooltip buttonTooltip = new Tooltip(props.getProperty(CHANGEBORDERTHICKNESS_TOOLTIP.toString()));
        thicknessSlider.setTooltip(buttonTooltip);
        reassignMapColors = app.getGUI().initChildButton(top, REASSIGN_MAP_COLORS.toString(), REASSIGNMAPCOLORS_TOOLTIP.toString(), false);
        playSubregionAnthem = app.getGUI().initChildButton(top, PLAY_SUB_REGION_ANTHEM.toString(), PLAYSUBREGIONANTHEM_TOOLTIP.toString(), false);
        Label zoom = new Label("  Zoom:   ");
        zoom.getStyleClass().add("cust");
        top.getChildren().add(zoom);
        zoomSlider = new Slider();
        zoomSlider.setMaxSize(0, 100);
        top.getChildren().add(zoomSlider);
        Tooltip buttonTooltip2 = new Tooltip(props.getProperty(ZOOMMAPINOUT_TOOLTIP.toString()));
        zoomSlider.setTooltip(buttonTooltip2);
        
        changeMapDimensions = app.getGUI().initChildButton(top, CHANGE_MAP_DIMENSIONS.toString(), CHANGEMAPDIMENSIONS_TOOLTIP.toString(), false);

        WorkspaceController workspaceController = new WorkspaceController(app);

        changeMapName.setOnAction(e -> workspaceController.handlerChangeMapName());

        addImageToMap.setOnAction(e -> workspaceController.handlerAddImageToMap());

        changeMapBackgroundColor.setOnAction(e -> workspaceController.changeBackgroundColor());

        changeBorderColor.setOnAction(e -> workspaceController.changeBorderColor());

        changeMapDimensions.setOnAction(e -> workspaceController.changeMapDimensions());
        
        left.setOnMouseReleased(e -> workspaceController.editSubregion());
        
        
        
        nameColumn = new TableColumn("Name");
        leaderColumn = new TableColumn("Leader");
        capitalColumn = new TableColumn("Capital");
        
//        nameColumn.getStyleClass().add("cust");
//        leaderColumn.getStyleClass().add("cust");
//        capitalColumn.getStyleClass().add("cust");
        
        
        
        nameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        leaderColumn.setCellValueFactory(new PropertyValueFactory<String, String>("leader"));
        capitalColumn.setCellValueFactory(new PropertyValueFactory<String, String>("capital"));
        DataManager dataManager = (DataManager) app.getDataComponent();

        itemsTable.getColumns().add(nameColumn);
        itemsTable.getColumns().add(leaderColumn);
        itemsTable.getColumns().add(capitalColumn);

        itemsTable.setItems(dataManager.getSubregions());
        
        itemsTable.getStyleClass().add("cust");

        Subregion item = itemsTable.getItems().get(0);
        
        splitPane.setDividerPositions(0.5);
        //left.setMinSize(app.getGUI().getPrimaryScene().getWidth() / 2, app.getGUI().getPrimaryScene().getHeight());
        left.getStyleClass().add("cust");
        
        //itemsTable.setMinSize(app.getGUI().getPrimaryScene().getWidth() / 2, app.getGUI().getPrimaryScene().getHeight());
        
        itemsTable.getStyleClass().add("cust");
        
        splitPane.setMinSize(app.getGUI().getPrimaryScene().getWidth(), app.getGUI().getPrimaryScene().getHeight());
        
        
        splitPane.getItems().addAll(left, itemsTable);
        workspace.getChildren().add(splitPane);
        
        
        itemsTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    
    public SplitPane getSplitPane() {

        return splitPane;
    }

}
