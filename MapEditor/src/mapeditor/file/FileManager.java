/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mapeditor.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import mapeditor.data.DataManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        
        loadAndRenderGeographicData(data, filePath);
        
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void loadAndRenderGeographicData(AppDataComponent data, String filePath) throws IOException {
    
                    DataManager dataManager = (DataManager) data;
        dataManager.reset();
        
        JsonObject json = this.loadJSONFile(filePath);

        int numberOfSubregions = Integer.parseInt(json.get("NUMBER_OF_SUBREGIONS").toString());

        int numberOfPolygons;
      
        JsonArray arrayOfSubregions = json.getJsonArray("SUBREGIONS");
      
        
        for (int i = 0; i < arrayOfSubregions.size(); i++) {

            JsonObject outterObject = arrayOfSubregions.getJsonObject(i);

            numberOfPolygons = Integer.parseInt(outterObject.get("NUMBER_OF_SUBREGION_POLYGONS").toString());
            
            
            for (int j = 0; j < 1; j++) {                                               

                JsonArray middleArray = outterObject.getJsonArray("SUBREGION_POLYGONS");
                
                
                for (int k = 0; k < middleArray.size(); k++) {
                    
                    Polygon polygon = new Polygon();
                    
                    JsonArray innerArray = middleArray.getJsonArray(k);
                    
                    
                    for (int z = 0; z < innerArray.size(); z++) {

                        double x = this.getDataAsDouble(innerArray.getJsonObject(z), "X");
                        double y = this.getDataAsDouble(innerArray.getJsonObject(z), "Y");
                        
                        double relativeX = ((DataManager) data).convertX(x);
                        
                        double relativeY = ((DataManager) data).convertY(y);
                        
                        polygon.getPoints().addAll(relativeX, relativeY);
                        
                    }
                    
                    polygon.setFill(Color.LIGHTGREEN);
                    polygon.setStroke(Color.BLACK);
                    ((DataManager) data).addPolygonToList(polygon);
                    
                }
            }

        }
    }
}
