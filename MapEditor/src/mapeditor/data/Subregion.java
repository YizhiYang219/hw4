/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mapeditor.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;

/**
 *
 * @author MATT
 */
public class Subregion {
    
    private final SimpleStringProperty name;
    private final SimpleStringProperty leader;
    private final SimpleStringProperty capital;
    
//    Image leaderImage;
//    Image flag;
    
    
    
    
    public Subregion(String name, String leader, String capital){
        
        this.name = new SimpleStringProperty(name);
        this.leader = new SimpleStringProperty(leader);
        this.capital = new SimpleStringProperty(capital);
    }
    
    public String getName(){
        return name.get();
    }
    
    public void setName(String name){
        this.name.set(name);
    }
    
    public StringProperty nameProperty(){
        return name;
    }
    
    public String getLeader(){
        return leader.get();
    }
    
    public void setLeader(String leader){
        this.leader.set(leader);
    }
    
    public StringProperty leaderProperty(){
        return leader;
    }
    
    public String getCapital(){
        return capital.get();
    }
    
    public void setCapital(String capital){
        this.capital.set(capital);
    }
    
    public StringProperty capitalProperty(){
        return capital;
    }
}
