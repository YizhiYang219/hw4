package mapeditor.data;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.FlowPane;
import javafx.scene.shape.Polygon;
import mapeditor.MapEditorApp;
import mapeditor.gui.Workspace;
import saf.components.AppDataComponent;

/**
 *
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {

    MapEditorApp app;

    ObservableList<Subregion> subregions;

    ArrayList<Polygon> polygons;

    public DataManager(MapEditorApp initApp) {

        app = initApp;

        subregions = FXCollections.observableArrayList();

        polygons = new ArrayList<Polygon>();
        
        Subregion region = new Subregion("GUANGZHOU", "YIZHI YANG", "YUEXIU");

        subregions.add(region);
    }

    @Override
    public void reset() {

    }

    public ObservableList getSubregions() {

        return subregions;
    }

    public void add(Subregion subregion) {

        subregions.add(subregion);
    }

    public double convertX(double x) {

        Workspace pane = (Workspace) app.getWorkspaceComponent();
        
        
        Double width = ((Workspace)app.getWorkspaceComponent()).getSplitPane().getWidth();
        
        double relativeX = (((width / 2) /360.0) * (180 + x));

        return relativeX;
    }

    public double convertY(double y) {

        Workspace pane = (Workspace) app.getWorkspaceComponent();

        FlowPane flow = (FlowPane) app.getGUI().getAppPane().getTop();

        double relativeY = (((app.getGUI().getPrimaryScene().getHeight() - flow.getHeight()) / 180) * (90 - y));

        return relativeY;
    }

    public void addPolygonToList(Polygon polygon) {

        polygons.add(polygon);
    }
    
    public ArrayList<Polygon> getPolygons(){
        
        return polygons;
    }
}
